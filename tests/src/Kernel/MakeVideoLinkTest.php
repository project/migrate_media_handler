<?php

namespace Drupal\Tests\migrate_media_handler\Kernel;

use Drupal\media\Entity\Media;
use Drupal\media\MediaInterface;

class MakeVideoLinkTest extends MediaMakerTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    $this->testMediaTypeSourcePlugin = 'oembed:video';
    $this->testMediaTypeValues = ['id' => 'remote_video'];
    parent::setUp();
  }

  /**
   * Test makeVideoLinkEntity().
   */
  public function testMakeVideoLinkEntity() {
    // Check basic assumptions about media working.
    $this->assertNotInstanceOf(MediaInterface::class, Media::load(rand(1000, 9999)));

    // Check video link creation.
    $data = [
      'video_url' => 'https://youtu.be/pqv_LUStxDw',
    ];
    $config = [
      'target_bundle' => $this->testMediaType->id(),
    ];
    $this->row->setSourceProperty('title', ['title text']);
    $this->row->setSourceProperty('language', ['en']);

    $video = $this->mediaMaker->makeVideoLinkEntity($data, $this->row, $config);
    $this->assertInstanceOf(Media::class, $video, 'Media entity not properly instantiated.');
    $this->assertInstanceOf(MediaInterface::class, Media::load($video->id()));

    $this->assertSame($this->testMediaType->id(), $video->bundle(), 'The media item was not created with the correct type.');
    $this->assertSame('title text', $video->getName(), 'The media item was not created with the correct name.');

    $this->assertSame((int) $data['video_url'], (int) $video->get('field_media_oembed_video')->value, 'The media item was not created with the correct link');





  }

}
