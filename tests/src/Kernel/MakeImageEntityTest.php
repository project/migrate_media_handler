<?php

namespace Drupal\Tests\migrate_media_handler\Kernel;

use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\media\MediaInterface;

class MakeImageEntityTest extends MediaMakerTestBase {

  /**
   * Test makeImageEntity().
   */
  public function testMakeImageEntity() {
    // Instantiate file for media entity.
    $file = $this->generateFile();
    $this->assertInstanceOf(File::class, $file, 'File not properly instantiated.');

    // Make and test Image entity.
    $config = [
      'target_bundle' => $this->testMediaType->id(),
      'source_field' => 'old_image_field',
    ];
    $old_image_field = [
      'alt' => 'alt text',
      'title' => 'title text',
    ];
    $this->row->setSourceProperty('old_image_field', [$old_image_field]);
    $image = $this->mediaMaker->makeImageEntity($file->id(), $this->row, $config);
    $this->assertInstanceOf(Media::class, $image, 'Media entity not properly instantiated.');

    $this->assertNotInstanceOf(MediaInterface::class, Media::load(rand(1000, 9999)));
    $this->assertInstanceOf(MediaInterface::class, Media::load($image->id()));

    $this->assertSame($this->testMediaType->id(), $image->bundle(), 'The media item was not created with the correct type.');
    $this->assertSame('test.jpg', $image->getName(), 'The media item was not created with the correct name.');
    $this->assertSame((int) $file->id(), (int) $image->get('field_media_image')->target_id, 'The media item was not created with the correct file');

    // Test alt and title.
    $this->assertSame('alt text', $image->get('field_media_image')->alt, "Alt text did not get saved.");
    $this->assertSame('title text', $image->get('field_media_image')->title, "Title text did not get saved.");

  }

}
