<?php

namespace Drupal\Tests\migrate_media_handler\Kernel;

use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;
use Drupal\media\MediaInterface;

class MakeAudioEntityTest extends MediaMakerTestBase {


  /**
   * {@inheritdoc}
   */
  protected function setUp():void {
    $this->testMediaTypeSourcePlugin = 'audio_file';
    parent::setUp();
  }

  /**
   * Test makeAudioEntity().
   */
  public function testMakeAudioEntity() {
    // Instantiate file for media entity.
    $this->testMediaFilename = 'test.wav';
    // Make & test file entity.
    $file = $this->generateFile();
    $this->assertInstanceOf(File::class, $file, 'File not properly instantiated.');

    // Make and test Document entity.
    $config = [
      'target_bundle' => $this->testMediaType->id(),
      'source_field' => 'old_audio_field',
    ];
    $old_audio_field = [
      'description' => 'description text',
      'display' => 'default',
    ];

    $this->row->setSourceProperty('old_audio_field', [$old_audio_field]);

    $document = $this->mediaMaker->makeAudioEntity($file->id(), $this->row, $config);
    $this->assertInstanceOf(Media::class, $document, 'Media entity not properly instantiated.');

    $this->assertNotInstanceOf(MediaInterface::class, Media::load(rand(1000, 9999)));
    $this->assertInstanceOf(MediaInterface::class, Media::load($document->id()));

    $this->assertSame($this->testMediaType->id(), $document->bundle(), 'The media item was not created with the correct type.');
    $this->assertSame('test.wav', $document->getName(), 'The media item was not created with the correct name.');
    $this->assertSame((int) $file->id(), (int) $document->get('field_media_audio_file')->target_id, 'The media item was not created with the correct file');
    $this->assertSame('description text', $document->get('field_media_audio_file')->description, "Description text did not get saved.");
    $this->assertSame('default', $document->get('field_media_audio_file')->display, "Display mode did not get saved.");

  }

}
