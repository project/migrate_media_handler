<?php

namespace Drupal\Tests\migrate_media_handler\Kernel;

use Drupal\media\Entity\Media;

class FindMediaTest extends MediaMakerTestBase {

  /**
   * Test findExistingMediaByHash().
   */
  public function testFindExistingMediaByHash() {
    // Generate media item.
    $media = $this->generateHashedMedia();
    // Check if it's a media item.
    $this->assertInstanceOf(Media::class, $media, 'Media not properly instantiated.');

    // Get media item by the recorded hash field.
    $hashed = $this->mediaMaker->findExistingMediaByHash($this->testMediaFilehash);
    // Verify it's a media item.
    $this->assertInstanceOf(Media::class, $hashed, 'Hashed media not properly instantiated.');

    // Verify the generated media item and the found item are the same.
    $this->assertSame($media->field_media_image->target_id, $hashed->field_media_image->target_id, 'Media and Hashed Media Differ.');
  }

  /**
   * Test findExistingMediaByPath().
   */
  public function testFindExistingMediaByPath() {
    // Generate media item.
    $media = $this->generateHashedMedia();
    // Check if it's a media item.
    $this->assertInstanceOf(Media::class, $media, 'Media not properly instantiated.');

    $found = $this->mediaMaker->findExistingMediaByPath($this->testMediaFilepath);
    // Verify it's a media item.
    $this->assertInstanceOf(Media::class, $found, 'Found media not properly instantiated.');

    // Verify the generated media item and the found item are the same.
    $this->assertSame($media->field_media_image->target_id, $found->field_media_image->target_id, 'Media and Found Media Differ.');

  }

  /**
   * Test findExistingMediaByPath().
   */
  public function testGetFileHash() {
    // Use generated media item.
    $this->generateHashedMedia();
    // Make sure generated hash is not a hash of empty string (da39...).
    $this->assertNotSame('da39a3ee5e6b4b0d3255bfef95601890afd80709', $this->testMediaFilehash);
    // Test mediaMaker's get hash function.
    $hash = $this->mediaMaker->getFileHash($this->testMediaFilepath);
    $this->assertSame($hash, $this->testMediaFilehash, "Incorrect hash");
    // Hash 00dc3... is the hash of vfs:// file specd in MediaMakerTestPase.php.
    $this->assertSame('00dc3a91d5ec3983f907020d265e10bb036a1ba2', $hash, "Incorrect hash");

    // Use remote image.
    $this->testMediaFilepath = 'https://www.drupal.org/files/issues/2021-11-10/posing.jpg';
    $this->testMediaFilehash = sha1_file($this->testMediaFilepath);
    // Make sure generated hash is not a hash of empty string (da39...).
    $this->assertNotSame('da39a3ee5e6b4b0d3255bfef95601890afd80709', $this->testMediaFilehash);
    // Test mediaMaker's get hash function.
    $hash = $this->mediaMaker->getFileHash($this->testMediaFilepath);
    $this->assertSame($hash, $this->testMediaFilehash);
  }

  public function testGetSourceFilePath() {
    // Test full path.
    $this->testMediaFilepath = 'http://www.example.com/sites/default/files/issues/2021-11-10/posing.jpg';
    $updated_path = $this->mediaMaker->getSourceFilePath($this->testMediaFilepath);
    $this->assertSame('public://d7store/issues/2021-11-10/posing.jpg', $updated_path, "File not found");

    // Test relative path.
    $this->testMediaFilepath = '/sites/default/files/issues/2021-11-10/posing.jpg';
    $updated_path = $this->mediaMaker->getSourceFilePath($this->testMediaFilepath);
    $this->assertSame('public://d7store/issues/2021-11-10/posing.jpg', $updated_path, "File not found");
  }

}
