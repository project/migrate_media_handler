<?php

namespace Drupal\Tests\migrate_media_handler\Kernel;

use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;
use Drupal\media\MediaInterface;

class MakeDocumentEntityTest extends MediaMakerTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp():void {
    $this->testMediaTypeSourcePlugin = 'file';
    $this->testMediaTypeValues = ['id' => 'document'];
    parent::setUp();
  }

  /**
   * Test makeDocumentEntity().
   */
  public function testMakeDocumentEntity() {
    // Instantiate file for media entity.
    $this->testMediaFilename = 'test.txt';
    $file = $this->generateFile();
    $this->assertInstanceOf(File::class, $file, 'File not properly instantiated.');

    // Make and test Document entity.
    $config = [
      'target_bundle' => $this->testMediaType->id(),
      'source_field' => 'old_document_field',
    ];
    $old_document_field = [
      'description' => 'description text',
      'display' => '1',
    ];

    $this->row->setSourceProperty('old_document_field', [$old_document_field]);

    $document = $this->mediaMaker->makeDocumentEntity($file->id(), $this->row, $config);
    $this->assertInstanceOf(Media::class, $document, 'Media entity not properly instantiated.');

    $this->assertNotInstanceOf(MediaInterface::class, Media::load(rand(1000, 9999)));
    $this->assertInstanceOf(MediaInterface::class, Media::load($document->id()));

    $this->assertSame($this->testMediaType->id(), $document->bundle(), 'The media item was not created with the correct type.');
    $this->assertSame('test.txt', $document->getName(), 'The media item was not created with the correct name.');
    $this->assertSame((int) $file->id(), (int) $document->get('field_media_file')->target_id, 'The media item was not created with the correct file');
    $this->assertSame('description text', $document->get('field_media_file')->description, "Description text did not get saved.");
    $this->assertSame('1', $document->get('field_media_file')->display, "Display mode did not get saved.");

  }

}
