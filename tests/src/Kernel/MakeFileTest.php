<?php

namespace Drupal\Tests\migrate_media_handler\Kernel;

use Drupal\file\Entity\File;

class MakeFileTest extends MediaMakerTestBase {

  /**
   * Test makeFileEntity().
   */
  public function testMakeFileEntity() {
    $file = $this->generateFile();
    $this->assertInstanceOf(File::class, $file, 'File not properly instantiated.');
  }

}
