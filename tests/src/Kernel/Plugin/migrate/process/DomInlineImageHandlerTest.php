<?php

namespace Drupal\Tests\migrate_media_handler\Kernel\migrate\process;

use Drupal\media\Entity\Media;
use Drupal\media\MediaInterface;
use Drupal\migrate_media_handler\Plugin\migrate\process\DomInlineImageHandler;
use Drupal\Tests\migrate_media_handler\Kernel\MediaMakerTestBase;
use Drupal\file\Entity\File;
use Drupal\Component\Utility\Html;
use org\bovigo\vfs\vfsStream;

class DomInlineImageHandlerTest extends MediaMakerTestBase {

  /**
   * Tests the dom_inline_image_handler process plugin.
   *
   * @coversDefaultClass \Drupal\migrate_media_handler\Plugin\migrate\process\DomInlineImageHandler
   * @group migrate_media_handler
   */
  public function testDomInlineImageHandler() {
    // Instantiate file for media entity.
    vfsStream::setup('drupal_root');
    vfsStream::create([
      'sites' => [
        'default' => [
          'files' => [
            $this->testMediaFilename => str_repeat('a', 3000),
          ],
        ],
      ],
    ]);

    $text = "Bruce Wayne, eccentric billionaire. I'm Batman. I'll be standing where I belong. Between you and the people of Gotham. My anger outweighs my guilt.<img src='" . $this->testMediaFilepath . $this->testMediaFilename . "'>It's not who I am underneath but what I do that defines me. Does it come in black?";
    $dom_obj = new \DOMDocument('1.0', 'UTF-8');
    $dom_obj->loadHTML($text);

    $configuration = [
      'target_bundle' => $this->testMediaType->id(),
    ];

    // Instantiate new process plugin.
    $class = new DomInlineImageHandler($configuration, 'dom_inline_image_handler', [], $this->mediaMaker, $this->configFactory);
    $this->assertInstanceOf(DomInlineImageHandler::class, $class, "DomInlineImageHandler Class not instantiated properly");

    // Run process plugin, test.
    $processed = $class->transform($dom_obj, $this->migrateExecutable, $this->row, '');
    $new_text =  Html::serialize($processed);

    $this->assertNotEquals($new_text, $text, "Text was not processed");
    $this->assertStringContainsString("<drupal-media data-entity-uuid=", $new_text, "drupal-media tag not found in processed text.");
    $this->assertStringContainsString("I'm Batman", $new_text,"Processed text probably overwritten.");

    // Load new media object from text, test.
    $uuid = substr($new_text, 182, 36);
    $image = $this->container->get('entity.repository')->loadEntityByUuid('media', $uuid);

    $this->assertInstanceOf(Media::class, $image, 'Media entity not properly instantiated.');
    $this->assertNotInstanceOf(MediaInterface::class, Media::load(rand(1000, 9999)));
    $this->assertInstanceOf(MediaInterface::class, Media::load($image->id()));
    $this->assertSame($this->testMediaType->id(), $image->bundle(), 'The media item was not created with the correct type.');

    // Test filename, alt and title.
    $this->assertSame('test.jpg', $image->getName(), 'The media item was not created with the correct name.');
    $this->assertSame('test.jpg', $image->get('field_media_image')->alt, "Alt text did not get saved.");
    $this->assertSame('test.jpg', $image->get('field_media_image')->title, "Title text did not get saved.");

    // Load File from media and check.
    $file = File::load($image->get('field_media_image')->target_id);
    $this->assertSame($this->testMediaFilename, $file->getFilename(), "Filename changed during processing");

  }

}