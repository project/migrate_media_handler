<?php

namespace Drupal\Tests\migrate_media_handler\Kernel\migrate\process;

use Drupal\media\Entity\Media;
use Drupal\media\MediaInterface;
use Drupal\migrate_media_handler\Plugin\migrate\process\UpdateFileToImage;
use Drupal\Tests\migrate_media_handler\Kernel\MediaMakerTestBase;
use Drupal\file\Entity\File;

class UpdateFileToImageTest extends MediaMakerTestBase {

  /**
   * Tests the update_file_to_image process plugin.
   *
   * @coversDefaultClass \Drupal\migrate_media_handler\Plugin\migrate\process\UpdateFileToImage
   * @group migrate_media_handler
   */
  public function testUpdateFileToImageSimple() {
    // Instantiate file for media entity.
    $file = $this->generateFile();
    $this->assertInstanceOf(File::class, $file, 'File not properly instantiated.');

    // Test basic image creation. Can't skip target_bundle bco test constraints.
    $configuration = [
      'target_bundle' => $this->testMediaType->id(),
    ];

    // Instantiate new process plugin.
    $class = new UpdateFileToImage($configuration, 'update_file_to_image', [], $this->mediaMaker);
    $this->assertInstanceOf(UpdateFileToImage::class, $class, "UpdateFileToImage Class not instantiated properly");

    // Run process plugin, test.
    $image = $class->transform($file->id(), $this->migrateExecutable, $this->row, '');

    $this->assertInstanceOf(Media::class, $image, 'Media entity not properly instantiated.');
    $this->assertNotInstanceOf(MediaInterface::class, Media::load(rand(1000, 9999)));
    $this->assertInstanceOf(MediaInterface::class, Media::load($image->id()));

    $this->assertSame($this->testMediaType->id(), $image->bundle(), 'The media item was not created with the correct type.');
    $this->assertSame('test.jpg', $image->getName(), 'The media item was not created with the correct name.');
    $this->assertSame((int) $file->id(), (int) $image->get('field_media_image')->target_id, 'The media item was not created with the correct file');
  }

  /**
   * Tests the update_file_to_image process plugin - alt and title migration.
   *
   * @coversDefaultClass \Drupal\migrate_media_handler\Plugin\migrate\process\UpdateFileToImage
   * @group migrate_media_handler
   */
  public function testUpdateFileToImageExtras() {
    // Instantiate file for media entity.
    $file = $this->generateFile();
    $this->assertInstanceOf(File::class, $file, 'File not properly instantiated.');

    // Test with alt and title.
    $configuration = [
      'source_field' => 'old_image_field',
      'target_bundle' => $this->testMediaType->id(),
    ];
    $old_image_field = [
      'alt' => 'alt text',
      'title' => 'title text',
    ];
    $this->row->setSourceProperty('old_image_field', [$old_image_field]);

    $class = new UpdateFileToImage($configuration, 'update_file_to_image', [], $this->mediaMaker);
    $image = $class->transform($file->id(), $this->migrateExecutable,$this->row, '');

    // Test alt and title.
    $this->assertSame('alt text', $image->get('field_media_image')->alt, "Alt text did not get saved.");
    $this->assertSame('title text', $image->get('field_media_image')->title, "Title text did not get saved.");

  }
}