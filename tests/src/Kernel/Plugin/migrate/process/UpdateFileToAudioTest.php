<?php

namespace Drupal\Tests\migrate_media_handler\Kernel\migrate\process;

use Drupal\media\Entity\Media;
use Drupal\media\MediaInterface;
use Drupal\migrate_media_handler\Plugin\migrate\process\UpdateFileToAudio;
use Drupal\Tests\migrate_media_handler\Kernel\MediaMakerTestBase;
use Drupal\file\Entity\File;

class UpdateFileToAudioTest extends MediaMakerTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp():void {
    $this->testMediaTypeSourcePlugin = 'audio_file';
    parent::setUp();
  }

  /**
   * Tests the update_file_to_audio process plugin.
   *
   * @coversDefaultClass \Drupal\migrate_media_handler\Plugin\migrate\process\UpdateFileToAudio
   * @group migrate_media_handler
   */
  public function testUpdateFileToAudioSimple() {
    // Instantiate file for media entity.
    $this->testMediaFilename = 'test.wav';
    $file = $this->generateFile();
    $this->assertInstanceOf(File::class, $file, 'File not properly instantiated.');

    // Test basic audio creation. Can't skip target_bundle bco test constraints.
    $configuration = [
      'target_bundle' => $this->testMediaType->id(),
    ];

    // Instantiate new process plugin.
    $class = new UpdateFileToAudio($configuration, 'update_file_to_audio', [], $this->mediaMaker);
    $this->assertInstanceOf(UpdateFileToAudio::class, $class, "UpdateFileToAudio Class not instantiated properly");

    // Run process plugin, test.
    $audio_id = $class->transform($file->id(), $this->migrateExecutable, $this->row, '');
    $this->assertEquals("1", $audio_id, "Plugin did not return an ID value.");

    $audio = Media::load($audio_id);
    $this->assertInstanceOf(Media::class, $audio, 'Media entity not properly instantiated.');
    $this->assertNotInstanceOf(MediaInterface::class, Media::load(rand(1000, 9999)));
    $this->assertInstanceOf(MediaInterface::class, $audio);

    $this->assertSame($this->testMediaType->id(), $audio->bundle(), 'The media item was not created with the correct type.');
    $this->assertSame('test.wav', $audio->getName(), 'The media item was not created with the correct name.');
    $this->assertSame((int) $file->id(), (int) $audio->get('field_media_audio_file')->target_id, 'The media item was not created with the correct file');
  }

  /**
   * Tests the update_file_to_audio process plugin - alt and title migration.
   *
   * @coversDefaultClass \Drupal\migrate_media_handler\Plugin\migrate\process\UpdateFileToAudio
   * @group migrate_media_handler
   */
  public function testUpdateFileToAudioExtras() {
    // Instantiate file for media entity.
    $file = $this->generateFile();
    $this->assertInstanceOf(File::class, $file, 'File not properly instantiated.');

    // Test with alt and title.
    $configuration = [
      'source_field' => 'old_audio_field',
      'target_bundle' => $this->testMediaType->id(),
    ];
    $old_audio_field = [
      'description' => 'description text',
      'display' => '1',
    ];
    $this->row->setSourceProperty('old_audio_field', [$old_audio_field]);

    $class = new UpdateFileToAudio($configuration, 'update_file_to_audio', [], $this->mediaMaker);
    $audio_id = $class->transform($file->id(), $this->migrateExecutable, $this->row, '');
    $this->assertEquals("1", $audio_id, "Plugin did not return an ID value.");

    $audio = Media::load($audio_id);

    // Test alt and title.
    $this->assertSame('description text', $audio->get('field_media_audio_file')->description, "Description text did not get saved.");
    $this->assertSame('1', $audio->get('field_media_audio_file')->display, "Display mode did not get saved.");

  }
}