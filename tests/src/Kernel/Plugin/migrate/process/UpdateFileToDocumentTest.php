<?php

namespace Drupal\Tests\migrate_media_handler\Kernel\migrate\process;

use Drupal\media\Entity\Media;
use Drupal\media\MediaInterface;
use Drupal\migrate_media_handler\Plugin\migrate\process\UpdateFileToDocument;
use Drupal\Tests\migrate_media_handler\Kernel\MediaMakerTestBase;
use Drupal\file\Entity\File;

class UpdateFileToDocumentTest extends MediaMakerTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp():void {
    $this->testMediaTypeSourcePlugin = 'file';
    $this->testMediaTypeValues = ['id' => 'document'];
    parent::setUp();
  }

  /**
   * Tests the update_file_to_document process plugin.
   *
   * @coversDefaultClass \Drupal\migrate_media_handler\Plugin\migrate\process\UpdateFileToDocument
   * @group migrate_media_handler
   */
  public function testUpdateFileToDocumentSimple() {
    // Instantiate file for media entity.
    $this->testMediaFilename = 'test.txt';
    $file = $this->generateFile();
    $this->assertInstanceOf(File::class, $file, 'File not properly instantiated.');

    // Test basic document creation. Can't skip target_bundle bco test constraints.
    $configuration = [
      'target_bundle' => $this->testMediaType->id(),
    ];

    // Instantiate new process plugin.
    $class = new UpdateFileToDocument($configuration, 'update_file_to_document', [], $this->mediaMaker);
    $this->assertInstanceOf(UpdateFileToDocument::class, $class, "UpdateFileToDocument Class not instantiated properly");

    // Run process plugin, test.
    $document_id = $class->transform($file->id(), $this->migrateExecutable, $this->row, '');
    $this->assertEquals("1", $document_id, "Plugin did not return an ID value.");

    $document = Media::load($document_id);
    $this->assertInstanceOf(Media::class, $document, 'Media entity not properly instantiated.');
    $this->assertNotInstanceOf(MediaInterface::class, Media::load(rand(1000, 9999)));
    $this->assertInstanceOf(MediaInterface::class, $document);

    $this->assertSame($this->testMediaType->id(), $document->bundle(), 'The media item was not created with the correct type.');
    $this->assertSame('test.txt', $document->getName(), 'The media item was not created with the correct name.');
    $this->assertSame((int) $file->id(), (int) $document->get('field_media_file')->target_id, 'The media item was not created with the correct file');
  }

  /**
   * Tests the update_file_to_document process plugin - alt and title migration.
   *
   * @coversDefaultClass \Drupal\migrate_media_handler\Plugin\migrate\process\UpdateFileToDocument
   * @group migrate_media_handler
   */
  public function testUpdateFileToDocumentExtras() {
    // Instantiate file for media entity.
    $file = $this->generateFile();
    $this->assertInstanceOf(File::class, $file, 'File not properly instantiated.');

    // Test with alt and title.
    $configuration = [
      'source_field' => 'old_document_field',
      'target_bundle' => $this->testMediaType->id(),
    ];
    $old_document_field = [
      'description' => 'description text',
      'display' => '1',
    ];
    $this->row->setSourceProperty('old_document_field', [$old_document_field]);

    $class = new UpdateFileToDocument($configuration, 'update_file_to_document', [], $this->mediaMaker);
    $document_id = $class->transform($file->id(), $this->migrateExecutable, $this->row, '');
    $this->assertEquals("1", $document_id, "Plugin did not return an ID value.");

    $document = Media::load($document_id);

    // Test alt and title.
    $this->assertSame('description text', $document->get('field_media_file')->description, "Description text did not get saved.");
    $this->assertSame('1', $document->get('field_media_file')->display, "Display mode did not get saved.");

  }
}