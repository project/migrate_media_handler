<?php

namespace Drupal\Tests\migrate_media_handler\Kernel\migrate\process;

use Drupal\media\Entity\Media;
use Drupal\media\MediaInterface;
use Drupal\migrate_media_handler\Plugin\migrate\process\UpdateFileToAudio;
use Drupal\migrate_media_handler\Plugin\migrate\process\UpdateLinkToVideo;
use Drupal\Tests\migrate_media_handler\Kernel\MediaMakerTestBase;
use Drupal\file\Entity\File;

class UpdateLinktoVideoTest extends MediaMakerTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp():void {
    $this->testMediaTypeSourcePlugin = 'oembed:video';
    $this->testMediaTypeValues = ['id' => 'remote_video'];
    parent::setUp();
  }

  /**
   * Tests the update_link_to_video process plugin.
   *
   * @coversDefaultClass \Drupal\migrate_media_handler\Plugin\migrate\process\UpdateLinkToVideo
   * @group migrate_media_handler
   */
  public function testUpdateLinkToVideo() {
    // Test basic video creation. Can't skip target_bundle bco test constraints.
    // Check video link creation.
    $data = [
      'video_url' => 'https://youtu.be/pqv_LUStxDw',
    ];
    $configuration = [
      'target_bundle' => $this->testMediaType->id(),
    ];

    // Instantiate new process plugin.
    $class = new UpdateLinkToVideo($configuration, 'update_link_to_video', [], $this->mediaMaker);
    $this->assertInstanceOf(UpdateLinkToVideo::class, $class, "UpdateLinkToVideo Class not instantiated properly");

    // Run process plugin, test.
    $video_id = $class->transform($data, $this->migrateExecutable, $this->row, '');
    $this->assertEquals("1", $video_id, "Plugin did not return an ID value.");

    $video = Media::load($video_id);
    $this->assertInstanceOf(Media::class, $video, 'Media entity not properly instantiated.');
    $this->assertNotInstanceOf(MediaInterface::class, Media::load(rand(1000, 9999)));
    $this->assertInstanceOf(MediaInterface::class, $video);

    $this->assertSame($this->testMediaType->id(), $video->bundle(), 'The media item was not created with the correct type.');
    $this->assertSame("The Lego Movie - Batman's Song (Untitled Self Portrait)", $video->getName(), "The media item was not created with the correct name.");
    $this->assertSame($data['video_url'], $video->get('field_media_oembed_video')->value, 'The media item was not created with the correct link');
  }

}