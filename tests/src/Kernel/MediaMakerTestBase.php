<?php

namespace Drupal\Tests\migrate_media_handler\Kernel;

use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\migrate\Row;
use Drupal\Tests\media\Kernel\MediaKernelTestBase;
use Drupal\field\Entity\FieldConfig;
use org\bovigo\vfs\vfsStream;

abstract class MediaMakerTestBase extends MediaKernelTestBase {

  /**
   * @var \Drupal\migrate_media_handler\MediaMaker
   */
  protected $mediaMaker;

  /**
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * @var \Drupal\migrate\Row|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $row;

  /**
   * @var \Drupal\migrate\MigrateExecutable|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $migrateExecutable;

  /**
   * @var string
   */
  protected $testMediaTypeSourcePlugin = 'image';

  /**
   * @var array
   */
  protected $testMediaTypeValues = [];

  /**
   * @var string
   */
  protected $testMediaFilename = 'test.jpg';

  /**
   * @var string
   */
  protected $testMediaFilepath = 'vfs://drupal_root/sites/default/files/';

  /**
   * @var string
   */
  protected $testMediaFilehash = '';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->configFactory = $this->container->get('config.factory');

    // Enable media and migration modules.
    $this->enableModules(['migrate_media_handler', 'text']);
    $this->installConfig(['migrate_media_handler', 'text']);
    $this->mediaMaker = $this->container->get('migrate_media_handler.mediamaker');

    $this->testMediaType = $this->createMediaType($this->testMediaTypeSourcePlugin, $this->testMediaTypeValues);

    // Create a field.
    $field_name = 'field_original_ref';
    $field_config = [
      'field_name' => $field_name,
      'entity_type' => 'media',
      'label' => 'Original Ref',
      'bundle' => $this->testMediaType->id(),
      'description' => '',
      'required' => FALSE,
      'settings' => [],
    ];
    FieldConfig::create($field_config)->save();

    // Fake up migration variables here to be able to use MediaMaker.
    $this->migrateExecutable = $this->createMock('Drupal\migrate\MigrateExecutable');
    // Can't use a mock migration Row, because mocks don't have methods, and we
    // need the setSourceValue method for some tests.
    $item = ['language' => 'en'];
    $this->row = new Row($item);

  }

  /**
   * Extend MediaKernelTestBase->generateMedia to include original ref field.
   *
   * @return \Drupal\media\Entity\Media
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function generateHashedMedia(): Media {
    $media = $this->generateMedia($this->testMediaFilename, $this->testMediaType);
    $this->testMediaFilehash = sha1_file($this->testMediaFilepath . $this->testMediaFilename);

    $media->set('field_media_image', $media->field_media_file);
    $media->set('field_original_ref', $this->testMediaFilehash);
    $media->save();
    return $media;
  }

  /**
   * Basic file creation.
   *
   * @return \Drupal\file\Entity\File
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */

  public function generateFile(): File {
    vfsStream::setup('drupal_root');
    vfsStream::create([
      'sites' => [
        'default' => [
          'files' => [
            $this->testMediaFilename => str_repeat('a', 3000),
          ],
        ],
      ],
    ]);
    // Make file entity.
    $file = $this->mediaMaker->makeFileEntity($this->testMediaFilepath . $this->testMediaFilename);
    return $file;
  }

}
