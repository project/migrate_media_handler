<?php

namespace Drupal\migrate_media_handler\Plugin\migrate\process;

use Drupal\Core\Config\ConfigFactory;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate_media_handler\MediaMaker;
use Drupal\migrate_plus\Plugin\migrate\process\DomProcessBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Takes inline ref docs from rich text and makes them into media entities.
 *
 * Meant to be used after dom process plugin.
 *
 * Has no config keys, but does use migrate_media_handler.settings.yml.
 *
 * Examples:
 *
 * @code
 * process:
 *   'body/value':
 *     -
 *       plugin: dom
 *       method: import
 *       source: 'body/0/value'
 *     -
 *       plugin: dom_inline_doc_handler
 *     -
 *       plugin: dom
 *       method: export
 * @endcode
 *
 * @see \Drupal\migrate\Plugin\MigrateProcessInterface
 * @see \Drupal\migrate_plus\Plugin\migrate\process\DomProcessBase
 *
 * @MigrateProcessPlugin(
 *   id = "dom_inline_doc_handler"
 * )
 */
class DomInlineDocHandler extends DomProcessBase implements ContainerFactoryPluginInterface {

  /**
   * Media Maker service.
   *
   * @var \Drupal\migrate_media_handler\MediaMaker
   */
  protected $mediaMaker;

  /**
   * ConfigFactory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Replace variable.
   *
   * @var string
   */
  protected $docReplace = '';

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, MediaMaker $media_maker, ConfigFactory $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->mediaMaker = $media_maker;
    $this->configFactory = $config_factory;

    // Pull media setting from config. Can be overridden if necessary.
    $config = $this->configFactory->get('migrate_media_handler.settings');
    $this->docReplace = $config->get('doc_replace');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('migrate_media_handler.mediamaker'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // Initialize DOM handling on this value.
    $this->init($value, $destination_property);

    // Loop through images to make into file/media entities and replace.
    $links = $this->xpath->query('//a');
    foreach ($links as $html_node) {
      // Get href attribute from the <a>.
      $href = $html_node->getAttribute('href');
      // Make sure the href is to a .pdf.
      if (stripos($href, '.pdf')) {
        // Using migrate_media_handler.settings, find the actual file.
        $source_file_path = $this->mediaMaker->getSourceFilePath($href);
        if (file_exists($source_file_path)) {
          // Check for existence of the file as a media entity already.
          $media = $this->mediaMaker->findExistingMediaByPath($source_file_path);
          // Initialize variable.
          $muuid = '';
          if ($media) {
            $muuid = $media->uuid();
          }
          // Otherwise, create document and media entities.
          else {
            $file = $this->mediaMaker->makeFileEntity($source_file_path);
            if ($file) {
              $media = $this->mediaMaker->makeDocumentEntity($file->id(), $row, $this->configuration);
              $muuid = $media->uuid();
            }
          }

          if (!empty($muuid)) {
            // Create a new DOM element for the document link in the text.
            $new_node = $this->document->createElement('drupal-media', "");
            // Add attributes to that element - start with uuid.
            $dom_att = $this->document->createAttribute('data-entity-uuid');
            $dom_att->value = $muuid;
            $new_node->appendChild($dom_att);

//            // Add caption
            $dom_att = $this->document->createAttribute('data-caption');
            $dom_att->value = $html_node->nodeValue;
            $new_node->appendChild($dom_att);

            // Add attributes from the migrate_media_handler.settings.
            // This allows for project-specific customization of media output.
            foreach ($this->docReplace as $attr => $val) {
              $dom_att = $this->document->createAttribute($attr);
              $dom_att->value = $val;
              $new_node->appendChild($dom_att);
            }
            // Replace the <a href> with <drupal-media>.
            $html_node->parentNode->replaceChild($new_node, $html_node);
          }
        }
      }
    }

    return $this->document;
  }

}
