<?php

namespace Drupal\migrate_media_handler\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate_media_handler\MediaMaker;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Convert existing file field references to media entities.
 *
 * Available config keys:
 * - source_field: the field on the entity with the file reference.
 * - target_bundle: the media type to which this gets migrated. Defaults to
 *   'audio'.
 *
 * Examples:
 *
 * Basic usage.
 * @code
 * process:
 *   field_audio:
 *     -
 *       plugin: migration_lookup
 *       source: field_old_audio
 *       migration: example_file
 *       no_stub: true
 *     -
 *       plugin: update_file_to_audio
 * @endcode
 *
 * A basic file->media migration can't include display & description, as that
 * information is not stored on the file, but is part of an audio/file field in
 * nodes or other entities. Update File to Audio allows you to use that entity
 * data to update the new media entity with display & description data.
 *
 *  Display & description usage.
 * @code
 * process:
 *   field_audio:
 *     -
 *       plugin: migration_lookup
 *       source: field_old_audio
 *       migration: example_file
 *       no_stub: true
 *     -
 *       plugin: update_file_to_audio
 *       source_field: field_old_audio
 * @endcode
 *
 * If you have your audios stored in a bundle other than 'audio', you can use
 * the target_bundle param to change which bundle they are saved in.
 *
 * @code
 * process:
 *   field_audio:
 *     -
 *       plugin: migration_lookup
 *       source: field_old_audio
 *       migration: example_file
 *       no_stub: true
 *     -
 *       plugin: update_file_to_audio
 *       target_bundle: recordings
 * @endcode
 *
 *
 * @see \Drupal\migrate\Plugin\MigrateProcessInterface
 *
 * @MigrateProcessPlugin(
 *   id = "update_file_to_audio"
 * )
 */
class UpdateFileToAudio extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The media maker.
   *
   * @var \Drupal\migrate_media_handler\MediaMaker
   */
  protected $mediaMaker;

  /**
   * Constructs a UpdateFileToDocument process plugin instance.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \Drupal\migrate_media_handler\MediaMaker $media_maker
   *   Media Maker service instance.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, MediaMaker $media_maker) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->mediaMaker = $media_maker;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('migrate_media_handler.mediamaker')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // If the migration lookup returned a file reference, make its media entity.
    if (!empty($value)) {
      $media = $this->mediaMaker->makeAudioEntity($value, $row, $this->configuration);
      return $media->id();
    }
    return NULL;
  }

}
