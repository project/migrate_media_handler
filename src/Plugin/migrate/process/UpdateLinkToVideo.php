<?php

namespace Drupal\migrate_media_handler\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate_media_handler\MediaMaker;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Convert existing file field references to media entities.
 *
 * Available config keys:
 * - target_bundle: the media type to which this gets migrated. Defaults to
 *   'remote_video'.
 *
 * Examples:
 *
 * Basic Usage:
 * @code
 * process:
 *   field_video:
 *     plugin: update_link_to_video
 * @endcode
 *
 * If you have your videos stored in a bundle other than 'remote_video', you can
 * use the target_bundle param to change which bundle they are saved in.
 *
 * @code
 * process:
 *   field_video:
 *     plugin: update_link_to_video
 *     target_bundle: vimeos
 * @endcode
 * @see \Drupal\migrate\Plugin\MigrateProcessInterface
 *
 * @MigrateProcessPlugin(
 *   id = "update_link_to_video"
 * )
 */
class UpdateLinkToVideo extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The media maker.
   *
   * @var \Drupal\migrate_media_handler\MediaMaker
   */
  protected $mediaMaker;

  /**
   * Constructs a UpdateFileToVideo process plugin instance.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \Drupal\migrate_media_handler\MediaMaker $media_maker
   *   Media Maker service instance.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, MediaMaker $media_maker) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->mediaMaker = $media_maker;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('migrate_media_handler.mediamaker')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // Make or find a video media entity.
    if (!empty($value)) {
      $media = $this->mediaMaker->makeVideoLinkEntity($value, $row, $this->configuration);
      if ($media) {
        return $media->id();
      }
    }
    return NULL;
  }

}
