<?php

namespace Drupal\migrate_media_handler\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate_media_handler\MediaMaker;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Record File Hash for later usage. Chains with migration_lookup for file data.
 *
 * Examples:
 *
 * @code
 * process:
 *   field_original_ref:
 *     -
 *       plugin: migration_lookup
 *       source: field_old_image
 *       migration: example_file
 *       no_stub: true
 *     -
 *       plugin: record_media_ref
 * @endcode
 *
 * @see \Drupal\migrate\Plugin\MigrateProcessInterface
 *
 * @MigrateProcessPlugin(
 *   id = "record_media_ref"
 * )
 */
class RecordMediaRef extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The media maker.
   *
   * @var \Drupal\migrate_media_handler\MediaMaker
   */
  protected $mediaMaker;

  /**
   * Constructs a RecordMediaRef process plugin instance.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \Drupal\migrate_media_handler\MediaMaker $media_maker
   *   Media Maker service instance.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, MediaMaker $media_maker) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->mediaMaker = $media_maker;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('migrate_media_handler.mediamaker')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // If the migration lookup returned a file reference, record file hash.
    if (!empty($value)) {
      $fileref = $this->mediaMaker->recordMediaRef($value, $row, $this->configuration);
      return $fileref;
    }
    return NULL;
  }

}
