# Migrate Media Handler
Moving files from Drupal 7 to Drupal 8 is a long, complicated process. There are multiple steps, and they differ by 
media type. This module simplifies the process by providing process plugin to handle common media migration tasks. 

This module provides process plugins to handle a few cases: Drupal 7 file fields, Drupal 7 video link fields, and
WYSIWYG embeds of `<img>` tags and `<a href='something.pdf'>` tags. 

In all cases, the intention is that these process plugins take data from the Drupal 7 source and do all the work to 
produce Drupal 8 media content. This means it will check for the existence of the file in the destination's file 
management, add it if not found, check for and create media entities, and supply new references or syntax as
appropriate.

## Step One: File Migration
Drupal's media structure requires that (nearly) all media types have a reference to a file entity.
So, pretty much all media migrations start with the file migration. Migrate Media Handler doesn't require any
customization of the [Drupal file migration](https://api.drupal.org/api/drupal/core%21modules%21file%21migrations%21d7_file.yml/8.8.x).

This documentation references the file migration as `example_file`. You can change that to the ID of your file 
migration.

## Step Two: Integrate Media to Migrate Media Handler
Enabling the Media module in Drupal creates four standard media entity bundles:
* Audio
* Document
* Image
* Video

When enabling Migrate Media Handler, a new text field called `field_original_ref` is added to each media entity type in 
the _target_ site. This field stores a hash of the filepath being referenced. This allows migration to lookup files by
file hash, avoiding duplication. This field will be deleted when you uninstall the module.
### Media Reference: Using the Record Media Ref Plugin
This plugin fills in the `field_original_ref` data with a hashed file path. This allows later entity migrations to
lookup media data by file path.
```yaml
'field_original_ref':
  -
    plugin: migration_lookup
    source: field_old_image
    migration: example_file
    no_stub: true
  -
    plugin: record_media_ref
```
This idea is borrowed from [Migrate File Singularity](https://www.drupal.org/sandbox/srjosh/3078435), a sandbox
migration module, which works on file migrations. 

Note that this plugin is new and might be a little buggy. If you run it and nothing is saved to the field, or an error
occurs, please let me know immediately.

## Step Three: Update Local Settings
Migrate Media Handler includes some settings that are used to manage things like file paths, site URI, and field names.
It is _highly_ advisable to review those, and reset them for your use case using `drush config-set`. 

This file can be found in `config/install/migrate_media_handler.settings.yml`.
* `site_uri`: Update this regex expression to match your site's (production) domain name. This is used to find and 
convert full-path links in rich text to media entities.
* `original_filepath`: Update this file path to reflect your original site's filepath (defaults to 
`/sites/default/files/`)
* `file_source`: Relative path (`public://` or `private://` format) where your source site's media assets live.
Put your image & document files here!
* `file_dest`: Relative path (`public://` or `private://` format) to which you are going to migrate your site's media
assets. Look for your image & document files here!
* `file_owner`: User ID of the file owner. Set to admin user 1 by default.
* `*_field_name`: Each media bundle will have a field containing the reference to a related file entity (or an oembed
link, in the case of video). By default, these are set to the default fieldnames used by Drupal 8 Media. You can update
them if necessary.
* `img_replace` and `doc_replace`: These settings represent attributes to be placed on the `<drupal-media>` embed tag
for images and documents, respectively. Used by DOM Inline Doc Handler and DOM Inline Image Handler process plugins.

If you need to make updates, ___Do Not Change This File___! Instead, override by using `drush config-set`, like so:
```shell script
drush config-set migrate_media_handler.settings site_uri '/https?:\/\/[w.]*drupal\.org/' -y
```
And then you can use `drush config-export` to save that setting in your configuration.

## Step Four: Entity Migrations
Migrate Media Handler operates on three types of fields: Rich Text, File Upload Fields, and Video Link fields. 

### Rich Text: Using DOM Plugins
Rich text field processing can look for pdf links and images in large text fields. There are two process plugins to
manage this:
* _DOM Inline Doc Handler_
* _DOM Inline Image Handler_

#### DOM Inline Doc Handler
This plugin extends DomProcessBase from _Migrate Plus_ and is designed to parse rich text blocks for existing `<a href>`
tags referencing documents (pdf only at this time), turning them into entity reference notation.
```yaml
'body/value':
  -
    plugin: dom
    method: import
    source: 'body/0/value'
  -
    plugin: dom_inline_doc_handler
  -
    plugin: dom
    method: export
```
Requires the `migrate_plus` module be enabled.

#### DOM Inline Image Handler
This plugin extends DomProcessBase from _Migrate Plus_ and is designed to parse rich text blocks for existing `<img>`
tags, turning them into entity reference notation.
```yaml
'body/value':
  -
    plugin: dom
    method: import
    source: 'body/0/value'
  -
    plugin: dom_inline_image_handler
  -
    plugin: dom
    method: export
```
Requires the `migrate_plus` module be enabled.

Note that it is possible to chain DOM plugins together, like so:
```yaml

'body/value':
  -
    plugin: dom
    method: import
    source: 'body/0/value'
  -
    plugin: dom_inline_doc_handler
  -
    plugin: dom_inline_image_handler
  -
    plugin: dom
    method: export
```

### File Fields: Using File Field Plugins
File Field processing handles three possible cases: Audio, document, and image fields. There are three process plugins
for this: 
* _Update File to Audio_
* _Update File to Document_
* _Update File to Image_

#### Update File to Audio
This plugin is designed to help you easily update audio upload file fields to media entity reference fields. It works in
concert with the core Migration Lookup plugin. Note that if `migration_lookup` does not find the file referenced,
`update_file_to_audio` will create a file entity with the source file, then a media entity referencing that file, then
return the new media entity id.
```yaml
field_audio:
  -
    plugin: migration_lookup
    source: field_old_audio
    migration: example_file
    no_stub: true
  -
    plugin: update_file_to_audio
```

#### Update File to Document
This plugin is designed to help you easily update document upload file fields to media entity reference fields. It works
in concert with the core Migration Lookup plugin. Note that if `migration_lookup` does not find the file referenced,
`update_file_to_document` will create a file entity with the source file, then a media entity referencing that file,
then return the new media entity id.
The `source_field` and `update_with_desc` parameters are optional; they update the display and description fields on the
document record.
```yaml
field_document:
  -
    plugin: migration_lookup
    source: field_old_document
    migration: example_file
    no_stub: true
  -
    plugin: update_file_to_document
    source_field: field_old_document
    update_with_desc: true
```

#### Update File to Image
This plugin is designed to help you easily update image upload file fields to media entity reference fields. It works in
concert with the core Migration Lookup plugin. Note that if `migration_lookup` does not find the file referenced,
`update_file_to_image` will create a file entity with the source file, then a media entity referencing that file, then
return the new media entity id.
The `source_field` and `update_with_alt` parameters are optional; they trigger an update of the alt and title fields on
the image record. The `target_bundle` parameter is also optional; it is used for instances where you need to save images in a bundle named something other than 'image'.
```yaml
field_image:
  -
    plugin: migration_lookup
    source: field_old_image
    migration: example_file
    no_stub: true
  -
    plugin: update_file_to_image
    source_field: field_old_image
    update_with_alt: true
    target_bundle: picture
```

### Video Fields: Using the Video Link Field Plugin
Link field processing handles a single case: Video link fields. There is one process plugin for this:
* _Update Link to Video_

#### Update Link to Video
This plugin works by taking the old YouTube URL field data, and converting it into a video media entity with proper oembed data. As there is no actual file reference here, there's no requirement to run `migration_lookup` as a part of this process. Nor does it have a dependency on a file migration.
```yaml
field_video:
  -
    plugin: update_link_to_video
    source: field_old_video
```
 
## Media Maker
There's a class file in this module called Media Maker (`src/MediaMaker.php`). All of the above plugins take advantage
of this file. MediaMaker centralizes the creation and manipulation of media entities for migration purposes. All methods
in this class are public, so it _could_ be used in other migration scripts.

## What's _NOT_ Included
This module is specifically for use cases where the Drupal 7 site did _not_ make use of Drupal 7 Media, in some or all
cases. It will not help with instances of media reference fields in Drupal 7, or embedded media using Drupal 7's node or
media embed functionality.

[Migrate File Singularity]: https://www.drupal.org/sandbox/srjosh/3078435
[Drupal file migration]: https://api.drupal.org/api/drupal/core%21modules%21file%21migrations%21d7_file.yml/8.8.x
