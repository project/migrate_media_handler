<?php

use Drupal\Core\Config\FileStorage;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Create fields from config stored in config/optional.
 *
 * @param array $entitytypes_fields
 *   An array of entity types fields you want to enable. E.g.
 * [
 *   'node' => 'field_to_make'
 * ]
 *
 * @see https://thinkshout.com/blog/2020/11/populating-fields/
 *
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function _ensure_fields(array $entitytypes_fields) {
  $module_path = \Drupal::service('module_handler')->getModule('migrate_media_handler')->getPath();
  $config_file = new FileStorage($module_path . '/config/optional');

  foreach ($entitytypes_fields as $entitytype => $field) {
    // Load up field storage config.
    $config_record = $config_file->read('field.storage.' . $entitytype . '.' . $field);
    FieldStorageConfig::loadByName($config_record['entity_type'], $config_record['field_name']);

    // Get bundles of that entity type.
    $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entitytype);
    foreach ($bundles as $bundle => $label) {
      // Load up field config
      $config_record = $config_file->read('field.field.' . $entitytype . '.' . $bundle . '.' . $field);
      if ($config_record) {
        FieldConfig::loadByName($config_record['entity_type'], $config_record['bundle'], $config_record['field_name']);
      }
    }
  }
}

/**
 * Implements hook_install().
 */
function migrate_media_handler_install() {
  $entitytype_fields = ['media' => 'field_original_ref'];
  _ensure_fields($entitytype_fields);
}

function migrate_media_handler_uninstall() {
  $field = FieldStorageConfig::loadByName('media', 'field_original_ref');
  if (!empty($field)) {
    $field->delete();
  }

}
